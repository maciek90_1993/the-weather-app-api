package com.theweatherappapi.event.registration;

import com.theweatherappapi.models.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

import java.util.Locale;

@Setter
@Getter
public class OnRegistrationCompleteEvent extends ApplicationEvent {
    private String appUrl;
    private Locale locale;
    private User user;

    public OnRegistrationCompleteEvent(User user, Locale locale, String appUrl){
        super(user);
        this.user = user;
        this.locale = locale;
        this.appUrl = appUrl;
    }
}
