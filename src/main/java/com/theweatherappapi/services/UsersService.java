package com.theweatherappapi.services;

import com.theweatherappapi.dto.UserDto;
import com.theweatherappapi.models.User;
import com.theweatherappapi.models.VerificationToken;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface UsersService {
    List<User> getAllUsers();
    void addListUsers(List<User> listUsers);
    void addUser(User user);
    User registerNewUserAccount(UserDto userDto);
    void createVerificationToken(User user, String token);
    VerificationToken getVerificationToken(String VerificationToken);
    void saveRegisteredUser(User user);
    User findUserByEmail(String email);
    void createPasswordResetTokenForUser(User user, String token);
    void changeUserPassword(User user, String password);
    Optional<User> getUserByPasswordResetToken(String token);
}


