package com.theweatherappapi.event.resetpassword;

import com.theweatherappapi.services.EmailService;
import com.theweatherappapi.services.UsersService;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.UUID;

@Component
@AllArgsConstructor
public class ResetPasswordListener implements ApplicationListener<OnResetPasswordEvent> {

    private final UsersService usersService;
    private final EmailService emailService;
    private final TemplateEngine templateEngine;

    @Override
    public void onApplicationEvent(OnResetPasswordEvent event) {
        this.resetPassword(event);
    }

    private void resetPassword(OnResetPasswordEvent event){
        String token = UUID.randomUUID().toString();
        usersService.createPasswordResetTokenForUser(event.getUser(), token);
        String subjectMail = "Resetowanie hasła.";
        String emailAdress = event.getUser().getEmail();
        String resetPasswordUrl  = event.getAppUrl() + "/changePassword?token=" + token;
        String contenMail = resetPasswordUrl;
        Context context = new Context();
        context.setVariable("link", contenMail);
        String body = templateEngine.process("resetpassword.html", context);
        emailService.sendEmail(emailAdress, subjectMail, contenMail, body, context);
    }
}
