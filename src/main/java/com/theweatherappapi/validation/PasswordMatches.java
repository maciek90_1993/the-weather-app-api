package com.theweatherappapi.validation;

import javax.validation.Payload;

public @interface PasswordMatches {
    String message() default "Passwords don't match";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
