package com.theweatherappapi.services.impl;

import com.theweatherappapi.models.User;
import com.theweatherappapi.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UsersRepository usersRepository;
    
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = usersRepository.findByemail(s);
        if(user == null) {
            throw new UsernameNotFoundException(s);
        }
            return org.springframework.security.core.userdetails.User.builder()
                    .password(user.getPassword())
                    .disabled(!user.isEnabled())
                    .username(user.getUsername())
                    .roles(user.getRole().name())
                    .build();
    }
}
