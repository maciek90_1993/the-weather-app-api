package com.theweatherappapi.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
@Builder
public class GenericResponse {

    private String message;
    private String error;
    private String token;

    public GenericResponse(String message){
        super();
        this.message = message;
    }
}
