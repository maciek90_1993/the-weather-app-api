package com.theweatherappapi.models;

public enum RolesEnum {
    USER("USER"),
    ADMIN("ADMIN");

    private String name;

    RolesEnum(String name){this.name = name;}
}
