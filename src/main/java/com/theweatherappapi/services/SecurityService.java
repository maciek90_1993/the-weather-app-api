package com.theweatherappapi.services;

import org.springframework.stereotype.Service;

@Service
public interface SecurityService {
    String validatePasswordResetToken(String token);
}