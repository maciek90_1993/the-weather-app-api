package com.theweatherappapi.services.impl;

import com.theweatherappapi.services.EmailService;
import lombok.AllArgsConstructor;
import org.springframework.core.io.ResourceLoader;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.ResourceUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;


import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileNotFoundException;


@Service
@AllArgsConstructor
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender javaMailSender;
    private final TemplateEngine templateEngine;
    private final ResourceLoader resourceLoader;
    @Override
    public void sendEmail(String to, String title, String content, String bodyHtml, Context context) {
        MimeMessage mail = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true, "UTF-8");
            helper.setTo(to);
            helper.setFrom("appsweather@gmail.com");
            helper.setSubject(title);
            helper.setText(bodyHtml, true);
            helper.addInline("sun.png", resourceLoader.getResource("classpath:images/sun.png"));
            helper.addInline("thermometer.png", resourceLoader.getResource("classpath:images/thermometer.png"));
            helper.addInline("raindrop.png", resourceLoader.getResource("classpath:images/raindrop.png"));
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        javaMailSender.send(mail);
    }
}
