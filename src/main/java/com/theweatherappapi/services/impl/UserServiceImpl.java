package com.theweatherappapi.services.impl;

import com.theweatherappapi.dto.UserDto;
import com.theweatherappapi.exception.UserAlreadyExistException;
import com.theweatherappapi.models.PasswordResetToken;
import com.theweatherappapi.models.RolesEnum;
import com.theweatherappapi.models.User;
import com.theweatherappapi.models.VerificationToken;
import com.theweatherappapi.repository.PasswordTokenRepository;
import com.theweatherappapi.repository.UsersRepository;
import com.theweatherappapi.repository.VerificationTokenRepository;
import com.theweatherappapi.services.UsersService;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UsersService {

    private final UsersRepository usersRepository;
    private final PasswordEncoder passwordEncoder;
    private final VerificationTokenRepository tokenRepository;
    private final PasswordTokenRepository passwordTokenRepository;

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public void addListUsers(List<User> listUsers) {
        for(User item: listUsers){
            usersRepository.save(item);
        }
    }

    @Override
    public void addUser(User user) {
        usersRepository.save(user);
    }

    @Transactional
    @Override
    public User registerNewUserAccount(UserDto userDto) throws UserAlreadyExistException {
        if(emailExist(userDto.getEmail())) {
            throw new UserAlreadyExistException("There is an account with that email address: "
                    +  userDto.getEmail());
        }
        User user = User.builder()
                .email(userDto.getEmail())
                .password(passwordEncoder.encode(userDto.getPassword()))
                .role(RolesEnum.USER)
                .username(userDto.getUsername())
                .build();
        usersRepository.save(user);
        return user;
    }

    @Override
    public void createVerificationToken(User user, String token) {
        VerificationToken myToken = new VerificationToken(token, user);
        tokenRepository.save(myToken);
    }

    @Override
    public VerificationToken getVerificationToken(String VerificationToken) {
        return tokenRepository.findByToken(VerificationToken);
    }

    @Override
    public void saveRegisteredUser(User user) {
        usersRepository.save(user);
    }

    @Override
    public User findUserByEmail(String email) {
        return usersRepository.findByemail(email) ;
    }

    @Override
    public void createPasswordResetTokenForUser(User user, String token) {
        PasswordResetToken myToken = new PasswordResetToken(token,user);
        passwordTokenRepository.save(myToken);
    }

    @Override
    public void changeUserPassword(User user, String password) {
        user.setPassword(password);
        usersRepository.save(user);
    }

    @Override
    public Optional<User> getUserByPasswordResetToken(String token) {
        return Optional.ofNullable(passwordTokenRepository.findBytoken(token).getUser());
    }

    private boolean emailExist(String email){
        return usersRepository.findByemail(email) != null;
    }
}
