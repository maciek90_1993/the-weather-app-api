package com.theweatherappapi.repository;

import com.theweatherappapi.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<User, Long> {
    User findByusername(String username);
    User findByemail(String email);
}
