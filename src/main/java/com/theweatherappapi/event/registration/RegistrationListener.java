package com.theweatherappapi.event.registration;

import com.theweatherappapi.models.User;
import com.theweatherappapi.services.EmailService;
import com.theweatherappapi.services.UsersService;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;


import java.util.UUID;

@Component
@AllArgsConstructor
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    private final EmailService emailService;
    private final UsersService usersService;
    private final TemplateEngine templateEngine;

    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(OnRegistrationCompleteEvent event){
        User user = event.getUser();
        String token = UUID.randomUUID().toString();
        usersService.createVerificationToken(user, token);
        String subjectMail  = "Potwierdź swój mail";
        String recipientAddress = user.getEmail();
        String confirmationUrl  = event.getAppUrl() + "/registrationConfirm?token=" + token;
        System.out.println(event.getLocale());
        String contenMail = confirmationUrl;
        Context context = new Context();
        context.setVariable("link", contenMail);
        String body = templateEngine.process("template.html", context);
        emailService.sendEmail(recipientAddress, subjectMail, contenMail, body, context);
    }
}
