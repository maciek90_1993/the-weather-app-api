package com.theweatherappapi.models;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name="users")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long userID;

    @Column(name = "username", nullable = false, unique = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "email", nullable = false, unique = false)
    private String email;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="weather_id", nullable = true)
    private Weather currentWeather;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private RolesEnum role;

    @Column(name = "enabled")
    private boolean enabled = false;
}
