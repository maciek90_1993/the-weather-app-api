package com.theweatherappapi.services;

import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

@Service
public interface EmailService {
    void sendEmail(String to, String title, String content, String bodyHtml, Context context);
}
