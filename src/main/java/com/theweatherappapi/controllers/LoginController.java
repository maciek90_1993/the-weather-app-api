package com.theweatherappapi.controllers;

import com.theweatherappapi.models.LoginCredentials;
import com.theweatherappapi.models.User;
import com.theweatherappapi.services.EmailService;
import com.theweatherappapi.services.UsersService;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@AllArgsConstructor
public class  LoginController {

    private final UsersService usersService;
    private final UserDetailsService userDetailsService;

    @RequestMapping("/")
    public String index(){
        return "Hello World !!!";
    }

    @RequestMapping("/singin")
    public UserDetails signIn(@RequestBody LoginCredentials loginCredentials, HttpServletResponse response){
        UserDetails userDetails = userDetailsService.loadUserByUsername(loginCredentials.getEmail());
        return userDetails;
    }

    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllUsers(){
        return new ResponseEntity<>(usersService.getAllUsers(), HttpStatus.OK);
    }
}
