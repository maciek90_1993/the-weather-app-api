package com.theweatherappapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class PasswordDto {

    private String oldPassword;
    private String token;
    private String newPassword;
}
