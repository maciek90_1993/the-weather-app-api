package com.theweatherappapi.repository;

import com.theweatherappapi.models.User;
import com.theweatherappapi.models.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerificationTokenRepository extends JpaRepository<VerificationToken,Long> {
    VerificationToken findByToken(String token);
    VerificationToken findByUser(User user);
}
