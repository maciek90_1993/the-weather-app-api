package com.theweatherappapi.controllers;

import com.theweatherappapi.dto.PasswordDto;
import com.theweatherappapi.dto.UserDto;
import com.theweatherappapi.event.registration.OnRegistrationCompleteEvent;
import com.theweatherappapi.event.resetpassword.OnResetPasswordEvent;
import com.theweatherappapi.exception.UserAlreadyExistException;
import com.theweatherappapi.exception.UserNotFoundException;
import com.theweatherappapi.models.User;
import com.theweatherappapi.models.VerificationToken;
import com.theweatherappapi.response.GenericResponse;
import com.theweatherappapi.services.SecurityService;
import com.theweatherappapi.services.UsersService;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.Locale;
import java.util.Optional;

@RestController
@AllArgsConstructor
public class RegistrationController {

    private final UsersService usersService;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final SecurityService securityService;
    private final MessageSource message;

    @PostMapping("/register")
    public ResponseEntity<Boolean> registerUserAccount(@RequestBody @Valid UserDto userDto,
                                                       BindingResult result,
                                                       HttpServletRequest request,
                                                       Errors errors){
        try{
            User registered = usersService.registerNewUserAccount(userDto);
            applicationEventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getLocale(), getAppUrl(request)));
        }catch (UserAlreadyExistException alreadyExistException){
            return new ResponseEntity<>(false , HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    @GetMapping("/registrationConfirm")
    public ResponseEntity<String> confirmRegistration(WebRequest request, @RequestParam("token") String token){
        Locale locale = request.getLocale();
        VerificationToken verificationToken = usersService.getVerificationToken(token);

        if(verificationToken == null) return new ResponseEntity<>("Bad verification token", HttpStatus.FORBIDDEN);
        User user = verificationToken.getUser();
        Calendar cal = Calendar.getInstance();
        if((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0)
            return new ResponseEntity<>("Token expired", HttpStatus.FORBIDDEN);
        user.setEnabled(true);
        usersService.saveRegisteredUser(user);
        return new ResponseEntity<>("Success user registration", HttpStatus.OK);
    }

    @PostMapping("/user/resetPassword")
    public GenericResponse resetPassword(HttpServletRequest request, @RequestParam("email") String userEmail){
        User user = usersService.findUserByEmail(userEmail);
        if(user == null) throw new UserNotFoundException();
        applicationEventPublisher.publishEvent(new OnResetPasswordEvent(getAppUrl(request), user));
        return new GenericResponse("Reset password");
    }

    @GetMapping("/changePassword")
    public GenericResponse showChangePasswordPage(Locale locale, @RequestParam("token") String token){
        String result = securityService.validatePasswordResetToken(token);
        if(result != null){
            String messageInfo = message.getMessage("auth.message." + result, null, locale);
            return new GenericResponse(messageInfo);
        } else {
            GenericResponse response = new GenericResponse(message.getMessage("auth.message.ok", null, locale));
            response.setToken(token);
            return response;
        }
    }

    @PostMapping("/savePassword")
    public GenericResponse savePassword(Locale locale, PasswordDto passwordDto){
        String result = securityService.validatePasswordResetToken(passwordDto.getToken());
        if(result != null){
            return new GenericResponse((message.getMessage(
                    "auth.message." + result, null, locale)));
        }
        Optional<User> user = usersService.getUserByPasswordResetToken(passwordDto.getToken());
        if(user.isPresent()){
            usersService.changeUserPassword(user.get(), passwordDto.getNewPassword());
            return new GenericResponse(message.getMessage(
                    "message.resetPasswordSuc", null, locale));
        } else {
            return new GenericResponse(message.getMessage(
                    "auth.message.invalid", null, locale));
        }
    }

    private String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
}
