package com.theweatherappapi.services.impl;

import com.theweatherappapi.models.PasswordResetToken;
import com.theweatherappapi.repository.PasswordTokenRepository;
import com.theweatherappapi.services.SecurityService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
@AllArgsConstructor
public class SecurityServiceImpl implements SecurityService {

    private final PasswordTokenRepository passwordTokenRepository;

    @Override
    public String validatePasswordResetToken(String token) {
        PasswordResetToken passwordResetToken = passwordTokenRepository.findBytoken(token);
        return !isTokenFound(passwordResetToken) ? "invalidToken"
                : isTokenExpired(passwordResetToken) ? "expired"
                : null;
    }
    private boolean isTokenFound(PasswordResetToken passToken) {
        return passToken != null;
    }

    private boolean isTokenExpired(PasswordResetToken passToken) {
        final Calendar cal = Calendar.getInstance();
        return passToken.getExpireDateToken().before(cal.getTime());
    }
}
