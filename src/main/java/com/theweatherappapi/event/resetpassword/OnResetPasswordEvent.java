package com.theweatherappapi.event.resetpassword;

import com.theweatherappapi.models.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class OnResetPasswordEvent extends ApplicationEvent {

    private String appUrl;
    private User user;

    public OnResetPasswordEvent(String appUrl, User user){
        super(user);
        this.user = user;
        this.appUrl = appUrl;
    }
}
