package com.theweatherappapi.models;


import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="weathers")
@AllArgsConstructor
public class Weather {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long weatherId;
    private String cityName;
    private String longitude;
    private String latitude;
    private String pressure;
    private String humidity;
    private String wind;
    private String clouds;
    private String rain;
    private String visibility;
    @OneToOne(mappedBy = "currentWeather")
    private User user;
}
